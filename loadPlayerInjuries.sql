 SET FOREIGN_KEY_CHECKS = 0;
  TRUNCATE reports_player_injuries;
 
 load data local infile 'C:/Users/Brett/Documents/FHM2 Reports Upload Folder/player_injuries.csv' replace into table reports_player_injuries
 fields terminated by ';'
 enclosed by '"'
 lines terminated by '\n'
 ignore 1 lines
 (playerId, teamId, franchiseId, injuryId, recoveryTime)