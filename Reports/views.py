from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import player_bio, league_data
from .models import team_data
from .models import player_ratings
from django.template import RequestContext, loader
from .models import player_skater_stats, player_goalie_stats, player_skater_career_stats, schedules, standings
from lib2to3.fixer_util import String
from .generalFunctions import *
from django.db.models.query import RawQuerySet
from django.db.models import Q


def index(request):
    player_list = player_bio.objects.order_by('playerId')[:2]
    output = ', '.join([p.name for p in player_list])
    return HttpResponse(output)

def leaguePage(request, leagueId):
    leagueContainer = get_object_or_404(league_data, pk=leagueId)
    #firstGoalsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId JOIN Reports_team_data ON Reports_team_data.teamId = Reports_player_skater_stats.teamId WHERE Reports_team_data.leagueId_id = %s AND Reports_player_bio.teamId_id > -1 ORDER BY Reports_player_skater_stats.goals DESC LIMIT 1', [leagueId])
    #firstAssistsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats LEFT JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId JOIN Reports_team_data ON Reports_team_data.teamId = Reports_player_skater_stats.teamId WHERE Reports_team_data.leagueId_id = %s AND Reports_player_bio.teamId_id > -1 ORDER BY Reports_player_skater_stats.assists DESC LIMIT 1', [leagueId])
    #topGoalsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId JOIN Reports_team_data ON Reports_team_data.teamId = Reports_player_skater_stats.teamId WHERE Reports_team_data.leagueId_id = %s AND Reports_player_bio.teamId_id > -1 ORDER BY Reports_player_skater_stats.goals DESC LIMIT 4 OFFSET 1', [leagueId])
    #topAssistsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats LEFT JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId JOIN Reports_team_data ON Reports_team_data.teamId = Reports_player_skater_stats.teamId WHERE Reports_team_data.leagueId_id = %s AND Reports_player_bio.teamId_id > -1 ORDER BY Reports_player_skater_stats.assists DESC LIMIT 4 OFFSET 1', [leagueId])    
    teamContainer = team_data.objects.filter(leagueId_id__exact=leagueId).order_by('name') 
    
    qJoin = player_skater_career_stats.objects.all().select_related('playerId').select_related('teamId').filter(leagueId__exact=leagueId).filter(type__exact='Regular Season')

    plusMinus = qJoin.order_by('year', 'plusMinus').reverse()[:5]
    assists = qJoin.order_by('year', 'a').reverse()[:5]    
    goals = qJoin.order_by('year', 'g').reverse()[:5] 
    
    stats = [goals, assists, plusMinus]
    statsText = ['g', 'a', 'plusMinus']
 
    context = {'leagueContainer':leagueContainer,
               #'topGoalsContainer':topGoalsContainer,
               #'topAssistsContainer':topAssistsContainer,
               'teamContainer':teamContainer,
               #'firstGoalsContainer':firstGoalsContainer,
               #'firstGoalsImage':firstGoalsImage,
               #'firstPlusMinus':firstPlusMinus,
               #'otherPlusMinus':otherPlusMinus,
               #'firstAssists':firstAssists,
               #'otherAssists':otherAssists,
               'stats':stats,
               'statsText':statsText,
               }
    return render(request, 'Reports/leaguePage.html', context)

def leagueStandingsPage(request, leagueId):
    numTeams = 14
    season = 1992

    games = schedules.objects.filter(leagueId__exact=leagueId).filter(y__in=[season, season+1]).filter(gameType__exact="Regular Season").filter(~Q(scoreHome = ''))

    teamRecordDict = {}
    current = 0
    #initialize the standings dictionary
    while current < numTeams:
        teamRecordDict[current] = [0,0,0,0,0,0] # W, L, T, P, GF, GA
        current += 1
        
    def listAdd(x, y):
        z = x
        i = 0
        while i < x.__len__():
            z[i] = x[i] + y[i]
            i += 1
        return z
        
    #loop through the querySet of regular season games
    for g in games:
        #who won?
        if g.scoreHome > g.scoreAway:
            #home won
            teamRecordDict[g.home_id] = listAdd(teamRecordDict[g.home_id], [1,0,0,3,int(g.scoreHome),int(g.scoreAway)])        
            teamRecordDict[g.away_id] = listAdd(teamRecordDict[g.away_id], [0,1,0,0,int(g.scoreAway),int(g.scoreHome)])  
        elif g.scoreAway > g.scoreHome:
            #away won
            teamRecordDict[g.home_id] = listAdd(teamRecordDict[g.home_id], [0,1,0,0,int(g.scoreHome),int(g.scoreAway)])        
            teamRecordDict[g.away_id] = listAdd(teamRecordDict[g.away_id], [1,0,0,3,int(g.scoreAway),int(g.scoreHome)]) 
        else:
            #tie
            teamRecordDict[g.home_id] = listAdd(teamRecordDict[g.home_id], [0,0,1,1,int(g.scoreHome),int(g.scoreAway)])        
            teamRecordDict[g.away_id] = listAdd(teamRecordDict[g.away_id], [0,0,1,1,int(g.scoreAway),int(g.scoreHome)])
    
    #commit standings updates to standings table
    teams = team_data.objects.filter(leagueId__exact=leagueId)
    for t in teams:
        r = standings(teamId=t, leagueId=league_data.objects.get(leagueId__exact=leagueId), season=season, w=teamRecordDict[t.teamId][0], l=teamRecordDict[t.teamId][1], t=teamRecordDict[t.teamId][2], p=teamRecordDict[t.teamId][3], gf=teamRecordDict[t.teamId][4], ga=teamRecordDict[t.teamId][5]) 
        r.save()
        #current += 1
    
    s = standings.objects.select_related('leagueId').select_related('teamId').filter(leagueId__exact=leagueId).filter(season__exact=season).order_by('p','w','t','gf').reverse()
    
    context = {'s':s,}       
    
    return render(request, 'Reports/standingsPage.html', context)        

def teamPage(request, teamId):
    teamContainer = get_object_or_404(team_data, pk=teamId)
    playerContainer = player_ratings.objects.select_related('playerId')
    playerContainer = playerContainer.filter(teamId__exact=teamId)
    firstGoalsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats LEFT JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId WHERE Reports_player_bio.teamId_id = %s ORDER BY Reports_player_skater_stats.goals DESC LIMIT 1', [teamId])
    firstAssistsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats LEFT JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId WHERE Reports_player_bio.teamId_id = %s ORDER BY Reports_player_skater_stats.assists DESC LIMIT 1', [teamId])
    topGoalsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats LEFT JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId WHERE Reports_player_bio.teamId_id = %s ORDER BY Reports_player_skater_stats.goals DESC LIMIT 4 OFFSET 1', [teamId])
    topAssistsContainer = player_skater_stats.objects.raw('SELECT * FROM Reports_player_skater_stats LEFT JOIN Reports_player_bio ON Reports_player_skater_stats.playerId = Reports_player_bio.playerId WHERE Reports_player_bio.teamId_id = %s ORDER BY Reports_player_skater_stats.assists DESC LIMIT 4 OFFSET 1', [teamId])
    leagueContainer = get_object_or_404(league_data, pk=teamContainer.leagueId_id)

    teamImage = teamImageFile(teamContainer.name, teamContainer.nickname)
    logo = leagueImageFile(leagueContainer.name)
    for x in firstGoalsContainer:    
        firstGoalsImage = playerImageFile(x.playerName, x.dateOfBirth)
        
    for x in firstAssistsContainer:    
        firstAssistsImage = playerImageFile(x.playerName, x.dateOfBirth)
    
    context = {'teamContainer':teamContainer,
               'playerContainer':playerContainer,
               'leagueContainer':leagueContainer,
               'teamFile':teamImage,
               'topGoalsContainer':topGoalsContainer,
               'topAssistsContainer':topAssistsContainer,
               'firstGoalsContainer':firstGoalsContainer,
               'firstGoalsImage':firstGoalsImage,
               'firstAssistsContainer':firstAssistsContainer,
               'firstAssistsImage':firstAssistsImage,
               'logo':logo,}
    return render(request, 'Reports/teamPage.html', context)

def playerPage(request, playerId):
    ratingsContainer = get_object_or_404(player_ratings, pk=playerId)
    bioContainer = get_object_or_404(player_bio, pk=playerId)
    teamContainer = get_object_or_404(team_data, pk=bioContainer.teamId_id)
    leagueContainer = get_object_or_404(league_data, pk=teamContainer.leagueId_id)
    #statsContainer = get_object_or_404(player_skater_stats, pk=playerId)
    #historicalStatsContainer = get_object_or_404(player_skater_career_stats, pk=playerId)

    playerImage = playerImageFile(bioContainer.playerName, bioContainer.dateOfBirth)
    teamImage = teamImageFile(teamContainer.name, teamContainer.nickname)

    stats = player_skater_career_stats.objects.all().select_related('teamIdPast')
    stats = stats.filter(playerId__exact=playerId).filter(type__exact='Regular Season').order_by('year').reverse()[:5]
    position = player_ratings.objects.get(pk=playerId).pos()
    
    shooting = int((ratingsContainer.shootingAccuracy + ratingsContainer.shootingRange) / 2)

    #template = loader.get_template('Reports/playerPageTemplate.html')
    context = {'ratingsContainer':ratingsContainer, 
               'bioContainer':bioContainer,
               'teamContainer':teamContainer,
               #'statsContainer':statsContainer,
               'leagueContainer':leagueContainer,
               'imageFile':playerImage,
               'teamFile':teamImage,
               'shooting':shooting,
               'stats':stats,
               'position':position,}
    #return HttpResponse("You're looking at player ", playerId, ". He is a ", ratingsContainer.lw, " at LW.")
    if position == 'G': return render(request, 'Reports/goaliePage.html', context)
    return render(request, 'Reports/playerPage.html', context)



