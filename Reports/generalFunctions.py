def playerImageFile(name, dob):
    #create image file name method
    parseHolder = ''
    firstNameFlag = False
    firstName = ''
    lastName = ''
    for char in name:
        if char != ' ':
            parseHolder = parseHolder + char
        else:
            if firstNameFlag == False:
                firstName = parseHolder
                parseHolder = ''
                firstNameFlag = True
    lastName = parseHolder
    parseHolder = ''
    
    yearFlag = False
    monthFlag = False
    day = ''
    month = ''
    year = ''
    for char in dob:
        if char != '-':
            parseHolder = parseHolder + char
        else:
            if yearFlag == False and monthFlag == False:
                yearFlag = True
                year = parseHolder
                parseHolder = ''
            elif yearFlag == True and monthFlag == False:
                monthFlag = True
                month = parseHolder
                parseHolder = ''
    day = parseHolder
    parseHolder = ''
    
    imageFile = ''
    
    imageFile = firstName + '_' + lastName + '_' + day + '_' + month + '_' + year + '.png'
    
    return imageFile
    
def teamImageFile(teamName, teamNickname):  
    
    parseHolder = ''
    
    for char in teamName:
        if char != ' ':
            parseHolder = parseHolder + char
        else:
            parseHolder = parseHolder + '_'
    parseHolder = parseHolder + '_'
    
    for char in teamNickname:
        if char != ' ':
            parseHolder = parseHolder + char
        else:
            parseHolder = parseHolder + '_'
    parseHolder = parseHolder + '.png'
    
    return parseHolder.lower()

def leagueImageFile(leagueName):
    
    parseHolder = ''
    
    for char in leagueName:
        if char != ' ':
            parseHolder = parseHolder + char
        else:
            parseHolder = parseHolder + '_'
    parseHolder = parseHolder + '.png'
    
    return parseHolder.lower()