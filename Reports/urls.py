'''
Created on Oct 4, 2015

@author: Brett
'''
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^Players/(?P<playerId>[0-9]+)/$', views.playerPage, name='playerPage'),
    url(r'^Teams/(?P<teamId>[0-9]+)/$', views.teamPage, name='teamPage'),
    url(r'^Leagues/(?P<leagueId>[0-9]+)/$', views.leaguePage, name='leaguePage'),
    url(r'^Leagues/(?P<leagueId>[0-9]+)/Standings$', views.leagueStandingsPage, name='leagueStandingsPage')
    
]