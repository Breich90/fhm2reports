from django.db import models
from .generalFunctions import *
from generalFunctions import leagueImageFile

class league_data(models.Model):
    leagueId = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=300)
    abbr = models.CharField(max_length=10)
    reputation = models.DecimalField(max_digits=3, decimal_places=2, default=0.00)
    def __str__(self):
        return self.name
    def image(self):
        return leagueImageFile(self.name)
    
class team_data(models.Model):
    teamId = models.IntegerField(primary_key=True)
    leagueId = models.ForeignKey(league_data)
    name = models.CharField(max_length=100)
    nickname = models.CharField(max_length=100)
    teamAbbr = models.CharField(max_length=10)
    parentTeam1 = models.IntegerField(default=-1)
    parentTeam2 = models.IntegerField(default=-1)
    parentTeam3 = models.IntegerField(default=-1)
    parentTeam4 = models.IntegerField(default=-1)
    parentTeam5 = models.IntegerField(default=-1)
    parentTeam6 = models.IntegerField(default=-1)
    parentTeam7 = models.IntegerField(default=-1)
    parentTeam8 = models.IntegerField(default=-1)
    primaryColor = models.CharField(max_length=7)
    secondaryColor = models.CharField(max_length=7)
    textColor = models.CharField(max_length=7)
    def __str__(self):
        return self.name + ' ' + self.nickname
    def image(self):
        return teamImageFile(self.name, self.nickname)

class player_bio(models.Model):
    playerId = models.IntegerField(primary_key=True)
    teamId = models.ForeignKey(team_data)
    franchiseId = models.IntegerField(default=-1)
    playerName = models.CharField(max_length=100)
    height = models.IntegerField(default=-1)
    weight = models.IntegerField(default=-1)
    age = models.IntegerField(default=-1)
    dateOfBirth = models.CharField(max_length=10)
    number = models.IntegerField(default=-1)
    contractLength = models.CharField(max_length=2, null=True)
    salary = models.CharField(max_length=15, null=True)
    ability = models.DecimalField(max_digits=4, decimal_places=2, default=0.0)
    potential = models.DecimalField(max_digits=4, decimal_places=2, default=0.0)
    def __str__(self):
        return self.name
    def image(self):
        return playerImageFile(self.playerName, self.dateOfBirth)
    
class player_ratings(models.Model):
    playerId = models.OneToOneField(player_bio, primary_key=True)
    teamId = models.ForeignKey(team_data)
    franchiseId = models.IntegerField(default=-1)
    g = models.IntegerField(default=-1)
    ld = models.IntegerField(default=-1)
    rd = models.IntegerField(default=-1)
    lw = models.IntegerField(default=-1)
    c = models.IntegerField(default=-1)
    rw = models.IntegerField(default=-1)
    aggression = models.IntegerField(default=-1)
    bravery = models.IntegerField(default=-1)
    determination = models.IntegerField(default=-1)
    teamplayer = models.IntegerField(default=-1)
    leadership = models.IntegerField(default=-1)
    temperament = models.IntegerField(default=-1)
    professionalism = models.IntegerField(default=-1)
    mentalToughness = models.IntegerField(default=-1)
    stamina = models.IntegerField(default=-1)
    acceleration = models.IntegerField(default=-1)
    agility = models.IntegerField(default=-1)
    balance = models.IntegerField(default=-1)
    speed = models.IntegerField(default=-1)
    stamina2 = models.IntegerField(default=-1)
    strength = models.IntegerField(default=-1)
    fighting = models.IntegerField(default=-1)
    screening = models.IntegerField(default=-1)
    gettingOpen = models.IntegerField(default=-1)
    passing = models.IntegerField(default=-1)
    puckHandling = models.IntegerField(default=-1)
    shootingAccuracy = models.IntegerField(default=-1)
    shootingRange = models.IntegerField(default=-1)
    offensiveRead = models.IntegerField(default=-1)
    checking = models.IntegerField(default=-1)
    faceoffs = models.IntegerField(default=-1)
    hitting = models.IntegerField(default=-1)
    positioning = models.IntegerField(default=-1)
    shotBlocking = models.IntegerField(default=-1)
    stickchecking = models.IntegerField(default=-1)
    defensiveRead = models.IntegerField(default=-1)
    gPositioning = models.IntegerField(default=-1)
    gPassing = models.IntegerField(default=-1)
    gPokechecking = models.IntegerField(default=-1)
    blocker = models.IntegerField(default=-1)
    glove = models.IntegerField(default=-1)
    rebound = models.IntegerField(default=-1)
    recovery = models.IntegerField(default=-1)
    gPuckhandling = models.IntegerField(default=-1)
    lowShots = models.IntegerField(default=-1)
    gSkating = models.IntegerField(default=-1)
    reflexes = models.IntegerField(default=-1)
    skating = models.IntegerField(default=-1)
    shooting = models.IntegerField(default=-1)
    playmaking = models.IntegerField(default=-1)
    defending = models.IntegerField(default=-1)
    physicality = models.IntegerField(default=-1)
    conditioning = models.IntegerField(default=-1)
    goalieCharacter = models.IntegerField(default=-1)
    hockeySense = models.IntegerField(default=-1)
    goalieTechnique = models.IntegerField(default=-1)
    goalieOverallPositioning = models.IntegerField(default=-1)
    def __str__(self):
        return self.name
    def pos(self):
        if self.g > 0:
            return 'G'
        if self.ld == 20:
            if self.rd > 14:
                return 'LD/RD'
            else:
                return 'LD'
        elif self.rd == 20:
            if self.ld > 14:
                return 'RD/LD'
            else:
                return 'RD'
        elif self.lw == 20:
            if self.c > 14:
                if self.rw > 14:
                    return 'LW/C/RW'
                else:
                    return 'LW/C'
            elif self.rw > 14:
                return 'LW/RW'
            else:
                return 'LW'
        elif self.c == 20:
            if self.rw > 14:
                if self.lw > 14:
                    return 'C/LW/RW'
                else:
                    return 'C/RW'
            elif self.lw > 14:
                return 'C/LW'
            else:
                return 'C'
        elif self.rw == 20:
            if self.lw > 14:
                if self.c > 14:
                    return 'RW/LW/C'
                else:
                    return 'RW/LW'
            elif self.c > 14:
                return 'RW/C'
            else:
                return 'RW'
                      
class player_skater_stats(models.Model):
    playerId = models.IntegerField(player_bio, primary_key=True)
    teamId = models.IntegerField(team_data)
    franchiseId = models.IntegerField(default=-1)
    gamesPlayed = models.IntegerField(default=-1)
    goals = models.IntegerField(default=-1)
    assists = models.IntegerField(default=-1)
    points = models.IntegerField(default=-1)
    plusMinus = models.IntegerField(default=-1)
    penaltyMinutes = models.IntegerField(default=-1)
    powerPlayGoals = models.IntegerField(default=-1)
    shorthandedGoals = models.IntegerField(default=-1)
    fights = models.IntegerField(default=-1)
    hits = models.IntegerField(default=-1)
    giveaways = models.IntegerField(default=-1)
    takeaways = models.IntegerField(default=-1)
    shotsBlocked = models.IntegerField(default=-1)
    PDO = models.IntegerField(default=-1)
    corsiRelative = models.IntegerField(default=-1)
    
class player_goalie_stats(models.Model):
    playerId = models.IntegerField(player_bio, primary_key=True)
    teamId = models.IntegerField(team_data)
    franchiseId = models.IntegerField(default=-1)
    gamesPlayed = models.IntegerField(default=-1)
    minutesPlayed = models.IntegerField(default=-1)
    wins = models.IntegerField(default=-1)
    losses = models.IntegerField(default=-1)
    ot = models.IntegerField(default=-1)
    shotsAgainst = models.IntegerField(default=-1)
    saves = models.IntegerField(default=-1)
    goalsAgainst = models.IntegerField(default=-1)
    goalsAgainstAverage = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    shutouts = models.IntegerField(default=-1)
    savePercentage = models.DecimalField(max_digits=4, decimal_places=3, default=0.0)

class injury_data(models.Model):
    injuryId = models.IntegerField(primary_key=True)
    injuryName = models.CharField(max_length=150)
    minDays = models.IntegerField(default=-1)
    maxDays = models.IntegerField(default=-1)
    
class player_injuries(models.Model):
    playerId = models.IntegerField(player_bio, primary_key=True)
    teamId = models.IntegerField(team_data)
    franchiseId = models.IntegerField(default=-1)
    injuryId = models.IntegerField(injury_data)
    recoveryTime = models.IntegerField(default=-1)
    
class schedules(models.Model):
    leagueId = models.IntegerField(league_data)
    gameId = models.IntegerField(default=-1)
    d = models.IntegerField(default=-1)
    m = models.IntegerField(default=-1)
    y = models.IntegerField(default=-1)
    home = models.ForeignKey(team_data, related_name="h")
    scoreHome = models.CharField(max_length=2)
    away = models.ForeignKey(team_data, related_name="a")
    scoreAway = models.CharField(max_length=2)
    gameType = models.CharField(max_length=20)
    
class standings(models.Model):
    teamId = models.OneToOneField(team_data, primary_key=True)
    leagueId = models.ForeignKey(league_data)
    season = models.IntegerField(default=0)
    w = models.IntegerField(default=0)
    l = models.IntegerField(default=0)
    t = models.IntegerField(default=0)
    p = models.IntegerField(default=0) 
    gf = models.IntegerField(default=0) 
    ga = models.IntegerField(default=0)
    def gd(self):
        return self.gf - self.ga      
    def gp(self):
        return self.w + self.l + self.t
    
class player_goalie_career_stats(models.Model):
    playerId = models.IntegerField(player_bio, primary_key=True)
    teamId = models.IntegerField(team_data)
    franchiseId = models.IntegerField(default=-1)    
    year = models.IntegerField(default=-1)
    teamIdPast = models.IntegerField(team_data)
    leagueId = models.IntegerField(league_data)
    gp = models.IntegerField(default=-1)  
    min = models.IntegerField(default=-1)  
    w = models.IntegerField(default=-1)  
    l = models.IntegerField(default=-1)  
    tol = models.IntegerField(default=-1)  
    eng = models.IntegerField(default=-1)  
    so = models.IntegerField(default=-1)  
    ga = models.IntegerField(default=-1)  
    sa = models.IntegerField(default=-1)  
    gr = models.IntegerField(default=-1)  
    type = models.CharField(max_length=20)
    
class player_skater_career_stats(models.Model):
    id = models.AutoField(default=-1, primary_key=True)
    playerId = models.ForeignKey(player_bio)
    teamId = models.ForeignKey(team_data, related_name="currentTeam")
    franchiseId = models.IntegerField(default=-1)    
    year = models.IntegerField(default=-1)
    teamIdPast = models.ForeignKey(team_data, related_name="pastTeam")
    leagueId = models.IntegerField(league_data)
    gp = models.IntegerField(default=-1)
    g = models.IntegerField(default=-1)
    a = models.IntegerField(default=-1)
    pim = models.IntegerField(default=-1)
    plusMinus = models.IntegerField(default=-1)
    powerPlayGoals = models.IntegerField(default=-1)
    powerPlayAssists = models.IntegerField(default=-1)
    shortHandedGoals = models.IntegerField(default=-1)
    shortHandedAssists = models.IntegerField(default=-1)
    gr = models.IntegerField(default=-1)
    gwg = models.IntegerField(default=-1)
    sog = models.IntegerField(default=-1)
    fo = models.IntegerField(default=-1)
    fow = models.IntegerField(default=-1)
    hit = models.IntegerField(default=-1)
    gva = models.IntegerField(default=-1)
    tka = models.IntegerField(default=-1)
    sb = models.IntegerField(default=-1)
    toi = models.IntegerField(default=-1)
    pptoi = models.IntegerField(default=-1)
    shtoi = models.IntegerField(default=-1)
    fights = models.IntegerField(default=-1)
    fightsWon = models.IntegerField(default=-1)
    type = models.CharField(max_length=20)

    
